#!/bin/bash

# shopt -s nullglob globstar

pass_dir=${PASSWORD_STORE_DIR-~/.password-store/}
ext=.gpg

pass_files=()
for p in $(find $pass_dir -type f -name "*.gpg"); do
  file=${p:${#pass_dir}:${#p}-${#pass_dir}-${#ext}}
  pass_files+=($file)
done

password=$(printf '%s\n' "${pass_files[@]}" | wofi -d "$@")

[[ -n $password ]] || exit

pass show -c "$password" 2>/dev/null
