from pathlib import Path
from argparse import ArgumentParser
import logging as log
import platform

def emacs_target_path(format):
    h = Path.home()
    if format == 'xdg':
        return h / '.config' / 'emacs'
    else:
        if platform.system() == 'Windows':
            return h / 'AppData' / 'Roaming' / '.emacs.d'
        else:
            return h / '.emacs.d'

def create(*dots, **kwargs):
    source = kwargs.get('source', Path.cwd())
    target = kwargs.get('target', Path.home() / '.config')
    paths = {}
    for dot in dots:
        sdot, ddot = dot if type(dot) is tuple else (dot, dot)
        paths[source / sdot] = target / ddot
    return paths

def link(dots):
    max_len = max(len(str(p)) for p in dots.keys())
    for source, target in dots.items():
        if source.exists():
            if target.exists():
                if target.is_symlink():
                    target.unlink()
                else:
                    log.warning(
                        f'{str(target)} is not a symlink, will not link'
                    )
                    continue
            target.parent.mkdir(parents=True, exist_ok=True)
            target.symlink_to(source)
            log.info(f'linked {str(source):{max_len}} => {str(target)}')
        else:
            log.warning(f'source {str(source)} does not exist; will not link')

def parse():
    parser = ArgumentParser(description='Deploy dotfiles')
    parser.add_argument(
        'action',
        choices=['show', 'link', 'list'],
        default='list',
        help='''
            `show` will preview the symbolic links;
            `list` print all the available configurations;
            `link` will create the symbolic links'''
    )
    parser.add_argument(
        '-c', '--configs',
        nargs='+',
        default=['emacs'],
        help='program configurations to link'
    )

    return parser.parse_args()

def pretty_print(dots):
    max_len = max(len(str(p)) for p in dots.keys())
    for s, d in dots.items():
        log.info(f'{str(s):{max_len}} => {str(d)}')

def list(dots):
    for c in dots.keys():
        log.info(str(c))
