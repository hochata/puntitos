-- setting basic properties
local set = vim.opt

set.relativenumber = true
set.termguicolors = true
set.ignorecase = true -- during autocompletion and search
set.infercase = true -- try to guess case in search
set.list = true -- show whitespace characters
set.number = true -- linenumbers
set.colorcolumn = '80'
set.hidden = true -- keeps abandoned buffers
set.mouse = 'a' -- enable mouse
set.showmode = false -- hide default bar
set.tabstop = 2
set.expandtab = true
set.shiftwidth = 2
set.smartindent = true
set.autoindent = true
set.clipboard = 'unnamedplus' -- copy and paste from system buffer
set.splitbelow = true
set.splitright = true
set.foldenable = false
set.wrap = false
set.completeopt = {'menu', 'menuone', 'noselect'}
vim.cmd 'syntax enable'

-- julia indentation
vim.cmd 'autocmd Filetype julia setlocal ts=4 sw=4 sts=0'

-- setting some keybindings
local map = vim.api.nvim_set_keymap

vim.g.mapleader = ' ' -- nice leader
map('t', '<esc>', [[<c-\><c-n>]], {noremap = true}) -- exiting a terminal

-- installing plugins
local Plug = vim.fn['plug#']

vim.call('plug#begin', vim.fn.stdpath('data') .. '/plugged')

-- best colorscheme
Plug 'shaunsingh/nord.nvim'

-- tsoding colorscheme
Plug 'rktjmp/lush.nvim'
Plug 'hwadii/gruber_darker.nvim'

-- autocomplete
Plug 'hrsh7th/cmp-buffer' -- buffer integration
Plug 'hrsh7th/nvim-cmp' -- completion engine

-- nicer file navigation
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

-- nicer way to do keybindings
Plug 'folke/which-key.nvim'

-- I'm lazy and want a nice status bar
Plug 'nvim-lualine/lualine.nvim'
Plug 'kyazdani42/nvim-web-devicons'

-- colors
Plug 'norcalli/nvim-colorizer.lua'

-- default haskell indentation is bad
Plug 'neovimhaskell/haskell-vim'

-- julia support
Plug 'JuliaEditorSupport/julia-vim'

-- nim support
Plug 'alaviss/nim.nvim'

-- toml syntax highlighting
Plug 'cespare/vim-toml'

-- nicer welcom
Plug 'mhinz/vim-startify'

-- crabs
Plug 'rust-lang/rust.vim'

Plug 'elixir-editors/vim-elixir'

vim.call('plug#end')

-- configuring autocomplete
local cmp = require'cmp'
cmp.setup{
  -- enter to confirm
  mapping = {
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
  },
  -- autocomplete using lsp and local buffer
  sources = {
    { name = 'buffer' },
  }
}

-- display color on files
require 'colorizer'.setup()

-- nord colorscheme config
vim.g.nord_contrast = false
vim.g.nord_borders = false
vim.g.nord_disable_background = true
vim.g.nord_italic = false

-- Load the colorscheme
-- require('nord').set()
vim.cmd 'colorscheme modus_operandi'

-- opacity for themes with no option
-- vim.cmd 'hi Normal guibg=NONE ctermbg=NONE'

-- enable icons
require'nvim-web-devicons'.setup{
  default = true
}

-- default status line
require'lualine'.setup{
  options = { theme = 'auto' },
}

-- which key configuration
local wk = require'which-key'
wk.setup{}

-- telescope keybindings
local tb = require'telescope.builtin'
wk.register(
  {
    name='file',
    ["."] = { tb.find_files, 'find file'},
    [","] = { tb.buffers, 'browse buffers'},
    g = {tb.live_grep, 'grep working directory'},
    h = { tb.help_tags, 'help tags'},
  },
  { prefix='<leader>' }
)

-- buffers bindings
 wk.register(
  {
    name='buffer',
    n = { ':bnext<cr>', 'next buffer' },
    m = { ':bprevious<cr>', 'prev buffer' },
    b = { ':bdelete!<cr>', 'delete buffer' },
  },
  { prefix='<leader>' }
)
