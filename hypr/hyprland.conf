# This is an example Hyprland config file.
#
# Refer to the wiki for more information.

#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#

# See https://wiki.hyprland.org/Configuring/Monitors/
monitor=,preferred,auto,1


# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = latam
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    repeat_rate = 65
    repeat_delay = 200
    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 5
    gaps_out = 10
    border_size = 1
    col.active_border = rgba(1affffee)
    col.inactive_border = rgba(595959aa)

    layout = dwindle
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 5
    blur = yes
    blur_size = 3
    blur_passes = 1
    blur_new_optimizations = on

    drop_shadow = yes
    shadow_range = 10
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = no

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 7, myBezier
    animation = windowsOut, 1, 7, default, popin 80%
    animation = border, 1, 10, default
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = on
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
device:epic mouse V1 {
    sensitivity = -0.5
}

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
windowrulev2 = float,class:^(firefox)$,title:^(Firefox — Sharing Indicator)$
windowrulev2 = move 48% 0%,class:^(firefox)$,title:^(Firefox — Sharing Indicator)$
windowrulev2 = float,class:^(pavucontrol),title:^(Volume Control)$$
windowrulev2 = float,class:^(com.gitlab.zehkira.Myuzi)$$
windowrulev2 = float,class:^(emacs),title:^(Yeq)
windowrulev2 = float,class:^(firefox),title:^(Developer Tools)
windowrulev2 = float,class:^(emacs),title:^(.*Minibuf.*)$
windowrulev2 = center,class:^(emacs),title:^(.*Minibuf.*)$

# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more


# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, Return, exec, foot
bind = $mainMod, Q, killactive, 
bind = $mainMod, C, exit,
bind = $mainMod, Z, fullscreen, 0
bind = $mainMod, F, togglefloating,
bind = $mainMod, D, exec, wofi --show drun --allow-images
bind = $mainMod, P, exec, passwofi
bind = $mainMod, E, exec, yequake-frames
bind = $mainMod, N, exec, swaync-client -t -sw
bind = , Print, exec, grim -g "$(slurp)" - | wl-copy

# Control pulse audio volume with pamixer (https://github.com/cdemoulins/pamixer)
bind = , XF86AudioRaiseVolume, exec, pamixer -i 5
bind = , XF86AudioLowerVolume, exec, pamixer -d 5
bind = , XF86AudioMute, exec, pamixer --toggle-mute

# Control MPRIS aware media players with playerctl (https://github.com/altdesktop/playerctl)
bind = , XF86AudioMedia, exec, playerctl play-pause
bind = , XF86AudioPlay, exec, playerctl play-pause
bind = , XF86AudioPrev, exec, playerctl previous
bind = , XF86AudioNext, exec, playerctl next
bind = , XF86AudioStop, exec, playerctl stop

# Control screen backlight brighness with light (https://github.com/haikarainen/light)
bind = , XF86MonBrightnessUp, exec, light -A 5
bind = , XF86MonBrightnessDown, exec, light -U 5


# dwindle bindings
bind = $mainMod, J, togglesplit,
bind = $mainMod, G, togglegroup,
bind = $mainMod, Tab, changegroupactive,

# will switch to a submap called resize
bind=$mainMod,R,submap,resize

# will start a submap called "resize"
submap=resize

# sets repeatable binds for resizing the active window
binde=,right,resizeactive,10 0
binde=,left,resizeactive,-10 0
binde=,up,resizeactive,0 -10
binde=,down,resizeactive,0 10
binde=$mainMod,right,moveactive,10 0
binde=$mainMod,left,moveactive,-10 0
binde=$mainMod,up,moveactive,0 -10
binde=$mainMod,down,moveactive,0 10

# use reset to go back to the global submap
bind=,escape,submap,reset 

# will reset the submap, meaning end the current one and return to the global one
submap=reset

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

# set wallpaper
exec-once = /home/quire/.azotebg

# set status bar
exec-once = waybar

# autostart some programs
exec-once = swaync # notification daemon

# automount usb
exec-once = udiskie --tray &

# battery daemon
exec-once  = batsignal -b -n BAT1 -w 30 -c 20 -d 10 -m 20 -D "loginctl suspend" &

# media server (should be run as user process)
exec-once = pipewire
exec-once = wireplumber

# networking
exec-once = nm-applet --indicator

# mimic pulse audio
exec-once = pipewire-pulse

# emacs server
exec-once = emacs --daemon

# screensharing
# https://github.com/emersion/xdg-desktop-portal-wlr/wiki/%22It-doesn't-work%22-Troubleshooting-Checklist#runit
exec-once = dbus-update-activation-environment DISPLAY I3SOCK SWAYSOCK WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=hyprland
exec-once = /usr/lib/xdg-desktop-portal -r

exec-once = swayidle -w timeout 180 'hyprctl dispatch dpms off' resume 'hyprctl dispatch dpms on' timeout 300 'loginctl suspend' before-sleep 'swaylock -f'
