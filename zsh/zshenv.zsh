export EDITOR=vi

export PATH="$HOME/.ghcup/bin:$PATH"
export PATH="$HOME/.nimble/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.luarocks/bin:$PATH"
export PATH="$HOME/.asdf/shims:$PATH"
export GOPATH=$HOME/Documents/repos

export CHROME_EXECUTABLE="/usr/bin/brave"
export CHROME_BIN="/usr/bin/brave"

export LEDGER_FILE=~/Documents/bs/hledger.journal
