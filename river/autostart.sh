#!/bin/sh

# import GTK config

import-gsettings &

# set wallpaper
~/.azotebg &

# set status bar
waybar &

# automount usb
udiskie --tray &

# networking
nm-applet --indicator &

# Idle configuration
swayidle -w \
     timeout 180 'wlr-randr --output "eDP-1" --off' \
     resume 'wlr-randr --output "eDP-1" --on' \
     timeout 300 'loginctl suspend' \
     before-sleep 'waylock -fork-on-lock' &

# media server (should be run as user process)
pipewire &
wireplumber &

# notifications
swaync &

# mimic pulse audio
pipewire-pulse &

# emacs server
emacs --daemon &

# screensharing
# https://github.com/emersion/xdg-desktop-portal-wlr/wiki/%22It-doesn't-work%22-Troubleshooting-Checklist#runit
dbus-update-activation-environment DISPLAY XAUTHORITY I3SOCK SWAYSOCK WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river &
