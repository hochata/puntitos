;;; flymake-gitlabci.el --- Flymake for Gitlab CI -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <hochata@disroot.org>
;; Keywords: vc, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Flymake integration with Gitlab linting for CI configuration files

;;; Code:

(require 'glab)

(defun flymake-gitlabi--try-follow-path (buff yaml-path)
  (let ((path (string-split yaml-path ":")))
    (with-current-buffer buff
      (save-excursion
	(goto-char (point-min))
	(cl-loop
	 for node in path
	 unless (re-search-forward (concat node ":") nil t)
	 return (cons (line-beginning-position) (line-end-position))
	 finally return (cons (line-beginning-position) (line-end-position)))))))

(defun flymake-gitlabci--guess-yaml-path (message)
  (cond
   ((string-match
     "jobs \\(.*\\) config should implement" message)
    (string-replace " " "_" (match-string 1 message)))
   ((string-match
     "jobs:\\(.*\\) config contains unknown keys: \\(.*\\)" message)
    (concat (match-string 1 message) ":" (match-string 2 message)))
   ((string-match
     "jobs:\\([^ ]+\\) \\(config \\)?should be a" message)
    (match-string 1 message))
   ((string-match
     "\\([^ ]+\\) job: chosen stage does not exist" message)
    (match-string 1 message))
   ((string-match
     "jobs:\\(.*\\) can't" message)
    (string-replace " " ":" (match-string 1 message)))
   (t "")))

(defun flymake-gitlabci--pos-for-message (buff mess)
  (if (string-match "line \\([[:digit:]]+\\) column \\([[:digit:]]+\\)" mess)
      (flymake-diag-region
       buff
       (string-to-number (match-string 1 mess))
       (string-to-number (match-string 2 mess)))
    (let ((path (flymake-gitlabci--guess-yaml-path mess)))
      (flymake-gitlabi--try-follow-path buff path))))

(defun flymake-gitlabci--request-diags (buff-str callback)
  (glab-post "/ci/lint"
	     nil
	     :payload `((content . ,buff-str))
	     :auth 'flymake-gitlabci
	     :errorback t
	     :callback callback))

(defun flymake-gitlabci--make-diags (locus type key diags)
  (let ((flymake-diags nil)
	(diag-texts (alist-get key diags)))
    (dolist (diag diag-texts flymake-diags)
      (pcase-let ((`(,beg . ,end) (flymake-gitlabci--pos-for-message locus diag)))
	(add-to-list
	 'flymake-diags
	 (flymake-make-diagnostic locus beg end type diag))))))

(defun flymake-gitlabci--process-diags (report-fn buffer req)
  (let* ((full-diags (ghub--req-value req))
	 (errors
	  (flymake-gitlabci--make-diags buffer :error 'errors full-diags))
	 (warns
	  (flymake-gitlabci--make-diags buffer :warning 'warnings full-diags)))
    (funcall report-fn (append errors warns))))

(defun flymake-gitlabci--check-buffer (report-fn buffer)
  (let ((buff-str (with-current-buffer buffer (buffer-string))))
    (flymake-gitlabci--request-diags
     buff-str
     (lambda (_ _ _ req)
       (flymake-gitlabci--process-diags report-fn buffer req)))))

(defun flymake-gitlabci-checker (report-fn &rest _options)
  (flymake-gitlabci--check-buffer
   report-fn
   (current-buffer)))

;;;###autoload
(defun flymake-gitlabci-enable ()
  (interactive)
  (add-to-list 'flymake-diagnostic-functions 'flymake-gitlabci-checker))

(provide 'flymake-gitlabci)
;;; flymake-gitlabci.el ends here
