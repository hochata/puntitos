;;; project-tabs.el --- create a tab per project     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <hochata@disroot.org>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Utility functions to associate tabs with projects, as well as switch and
;; close them.

;;; Code:

(require 'project)
(require 'tab-bar)

(defun project-tabs--list (&optional frame)
  (let* ((curr-proj (project-current))
	(curr-proj-name (when curr-proj (project-name curr-proj)))
	(tabs (frame-parameter frame 'project-tabs)))
    (mapcar (lambda (tab)
	      (let* ((tab-name (alist-get 'name tab)))
		`(,(if (string= tab-name curr-proj-name) 'current-tab 'tab)
		  (name . ,tab-name))))
	    tabs)))

(defun project-tabs--new (&optional arg)
  (let* ((proj-name (project-name (project-current t)))
	 (frame-tabs (frame-parameter nil 'project-tabs))
	 (new-tab `((name . ,proj-name))))
    (unless frame-tabs
      (project-tabs--enable-tabs-in-frame))
    (set-frame-parameter
     nil
     'project-tabs
     (cl-pushnew new-tab frame-tabs :test #'equal))
    new-tab))

;;;###autoload
(defun project-tabs-switch (dir)
  (interactive (list (project-prompt-project-dir)))
  (let* ((project-current-directory-override dir)
	 (proj-name (project-name (project-current)))
	 (frame-tabs (frame-parameter nil 'project-tabs)))
    (project-switch-project dir)
    (tab-bar-switch-to-tab proj-name)))

(defun project-tabs--disable-tabs-in-frame (&optional frame)
  (set-frame-parameter frame 'tab-bar-lines 0)
  (set-frame-parameter frame 'tab-bar-lines-keep-state nil))

(defun project-tabs--enable-tabs-in-frame (&optional frame)
  (set-frame-parameter frame 'tab-bar-lines 1)
  (set-frame-parameter frame 'tab-bar-lines-keep-state t))

;;;###autoload
(define-minor-mode project-tabs-mode
  "Use tabs to show the currently open project"
  :global t
  (if project-tabs-mode
      (progn
	(when (frame-parameter nil 'project-tabs)
	  (project-tabs--enable-tabs-in-frame))
	(advice-add 'tab-bar-new-tab-to :override #'project-tabs--new)
	(setq tab-bar-tabs-function #'project-tabs--list))
    (advice-remove 'tab-bar-new-tab #'project-tabs--new)
    (project-tabs--disable-tabs-in-frame)))

(provide 'project-tabs)
;;; project-tabs.el ends here
