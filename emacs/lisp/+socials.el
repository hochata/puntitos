;;; +socials.el --- interacting with social networks  -*- lexical-binding: t; -*-

;;; Commentary:

;; Configuring Emacs integration into some social networks.

;;; Code:

(setopt message-send-mail-function 'smtpmail-send-it
	message-kill-buffer-on-exit t)

(setopt smtpmail-smtp-service 465
	smtpmail-stream-type 'ssl
	smtpmail-servers-requiring-authorization ".*"
	smtpmail-local-domain
	(car (split-string (shell-command-to-string "hostname"))))

(setopt gnus-thread-hide-subtree t
	gnus-summary-make-false-root 'adopt
	gnus-secondary-select-methods '((nntp "news.gmane.io"))
	gnus-check-new-newsgroups nil
	gnus-message-archive-group
	'((if (message-news-p)
	      (format-time-string "sent.%Y-%m")
	    "Sent")))

(unless (package-installed-p 'org-mime)
  (package-install 'org-mime))

(unless (package-installed-p 'mbsync)
  (package-install 'mbsync))

(add-hook 'gnus-startup-hook 'mbsync)

(add-hook 'mbsync-exit-hook 'gnus-group-get-new-news)

(with-eval-after-load 'gnus-group
  (define-key gnus-group-mode-map (kbd "f") 'mbsync))

;;(gnus-demon-add-handler 'mbsync 2 t)
;; (gnus-demon-init)

(unless (package-installed-p 'mastodon)
  (package-install 'mastodon))

(with-eval-after-load 'mastodon
  (setq mastodon-toot--enable-custom-instance-emoji t
	mastodon-tl--show-avatars t
	mastodon-tl--enable-proportional-fonts t
	mastodon-media--enable-image-caching t))

(add-hook 'mastodon-mode-hook 'visual-fill-column-mode)

(with-eval-after-load '+buffers
  (+yeq-add "Masto" 'mastodon))

(provide '+socials)
;;; +socials.el ends here
