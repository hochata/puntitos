;;; +julia.el --- julia ide conf                     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <hochata@disroot.org>
;; Keywords: languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(unless (package-installed-p 'julia-ts-mode)
  (package-install 'julia-ts-mode))

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(julia . ("https://github.com/tree-sitter/tree-sitter-julia"))))

(unless (package-installed-p 'eglot-jl)
  (package-install 'eglot-jl))

(unless (package-installed-p 'julia-repl)
  (package-install 'julia-repl))

(add-hook 'julia-ts-mode-hook 'eglot-jl-init)
(add-hook 'julia-ts-mode-hook 'julia-repl-mode)

(provide '+julia)
;;; +julia.el ends here
