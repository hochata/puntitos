;;; +secrets.el --- interaction with password managers  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <hochata@disroot.org>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(setq auth-source-do-cache t)

(if (eq system-type 'darwin)
    (progn
      (unless (package-installed-p 'bitwarden)
	(package-install 'bitwarden))

      (setq bitwarden-bw-executable "/opt/homebrew/bin/bw")
      (setq bitwarden-automatic-unlock
	    (lambda ()
	      (read-passwd "Bitwarden master password: ")))
      (bitwarden-auth-source-enable))
  (unless (package-installed-p 'password-store)
    (package-install 'password-store))
  (auth-source-pass-enable))

(provide '+secrets)
;;; +secrets.el ends here
