;;; +lua.el --- support for lua scripts              -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <edgarquiroz@192.168.1.5>
;; Keywords: languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(lua . ("https://github.com/MunifTanjim/tree-sitter-lua"))))

(setq lua-ts-indent-offset 2
      lua-ts-luacheck-program "/opt/homebrew/bin/luacheck")

(add-to-list 'auto-mode-alist '("\\.lua\\'" . lua-ts-mode))

(add-hook 'lua-ts-mode-hook (lambda () (indent-tabs-mode -1)))

(unless (package-installed-p 'hare-mode)
  (package-vc-install "https://git.sr.ht/~bbuccianti/hare-mode"))

(provide '+lua)
;;; +lua.el ends here
