;;; +zig.el --- zig support                          -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(unless (package-installed-p 'zig-mode)
  (package-install 'zig-mode))

(provide '+zig)
;;; +zig.el ends here
