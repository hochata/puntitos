;;; +golang.el --- go specific configuration     -*- lexical-binding: t; -*-

;;; Commentary:

;; Treesitter, LSP, Project integration.

;;; Code:

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(go . ("https://github.com/tree-sitter/tree-sitter-go" "v0.20.0")))
  (add-to-list 'treesit-language-source-alist
	       '(gomod . ("https://github.com/camdencheek/tree-sitter-go-mod"))))

(setq go-ts-mode-indent-offset 8)
(add-hook 'go-ts-mode-hook 'indent-tabs-mode)

(add-hook 'go-ts-mode-hook 'apheleia-mode-maybe)

(add-to-list 'auto-mode-alist '("\\.go\\'" . go-ts-mode))
(add-to-list 'auto-mode-alist '("go\\.mod" . go-mod-ts-mode))

(with-eval-after-load 'compile
  (add-to-list 'compilation-error-regexp-alist 'go-testify)
  (add-to-list
   'compilation-error-regexp-alist-alist
   '(go-testify
     "^[[:blank:]]+Error Trace:[[:blank:]]+\\(.+\\):\\([0-9]+\\).*$"
     1 2)))

(with-eval-after-load 'project
  (add-to-list
   'project-vc-extra-root-markers
   "go.mod"))

(provide '+golang)
;;; +golang.el ends here
