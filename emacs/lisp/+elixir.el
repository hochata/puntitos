;;; +elixir.el --- elixir ide                    -*- lexical-binding: t; -*-

;;; Commentary:

;; Treesitter, IEx, elixir-ls, Credo, Mix and whatever I deem useful for
;; Elixir development.

;;; Code:

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(elixir "https://github.com/elixir-lang/tree-sitter-elixir"))
  (add-to-list 'treesit-language-source-alist
	       '(heex  "https://github.com/phoenixframework/tree-sitter-heex")))

(unless (package-installed-p 'elixir-ts-mode)
  (package-install 'elixir-ts-mode))

(add-hook 'heex-ts-mode-hook (lambda () (indent-tabs-mode -1)))
(add-hook 'auto-mode-alist '("\\.[h]?eex\\'" . heex-ts-mode))

(add-hook 'elixir-ts-mode-hook (lambda () (indent-tabs-mode -1)))
(add-to-list 'auto-mode-alist '("\\.ex[s]?\\'" . elixir-ts-mode))
(add-to-list 'auto-mode-alist '("mix\\.lock" . elixir-ts-mode))

(with-eval-after-load 'elixir-ts-mode
  (define-key elixir-ts-mode-map (kbd "C-c C-z") '+project-iex))

(unless (package-installed-p 'eglot-elixir)
  (package-vc-install '(eglot-elixir :url "https://github.com/bvnierop/eglot-elixir/")))

(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs '(elixir-ts-mode . eglot-elixir))
  (add-to-list 'eglot-server-programs '(heex-ts-mode . eglot-elixir)))

(defun +project-iex ()
  (interactive)
  (let* ((default-directory (project-root (project-current t))))
      (eat "iex -S mix")))

(with-eval-after-load 'project
  (add-to-list
   'project-vc-extra-root-markers
   "mix.exs"))

(unless (package-installed-p 'mix)
  (package-install 'mix))

(with-eval-after-load 'mix
  (define-key mix-minor-mode-map (kbd "C-c") 'mix-minor-mode-command-map))

(add-hook 'elixir-ts-mode-hook 'mix-minor-mode)

(provide '+elixir)
;;; +elixir.el ends here
