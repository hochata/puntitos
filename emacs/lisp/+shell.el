;;; +shell.el --- posix shell interaction            -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <edgarquiroz@192.168.1.3>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(unless (package-installed-p 'eat)
  (package-install 'eat))

(add-hook 'eshell-load-hook #'eat-eshell-mode)

(with-eval-after-load 'project
  (define-key project-prefix-map (kbd "t") 'eat-project))

;; No idea why TERMINFO lookup is failing
(when (equal system-type 'darwin)
  (setq eat-term-name "eat-color"))

(unless (package-installed-p 'exec-path-from-shell)
  (package-install 'exec-path-from-shell))

(when (daemonp)
  (exec-path-from-shell-initialize))

(defun +shell-send-async-region ()
  (interactive)
  (let ((max-mini-window-height 1)
	(async-shell-command-buffer 'new-buffer))
  (async-shell-command
   (buffer-substring
    (region-beginning)
    (region-end)))))

(setopt async-shell-command-buffer 'new-buffer)

(with-eval-after-load 'markdown-mode
  (define-key markdown-mode-map (kbd "C-c C-e") '+shell-send-async-region))

(provide '+shell)
;;; +shell.el ends here
