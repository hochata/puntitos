;;; +faces.el --- faces, themes, look and feel       -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(set-face-attribute 'default nil
		    :family "NotoSansM NF"
		    :height 110)
(set-face-attribute 'fixed-pitch nil
		    :family "NotoSansM NF"
		    :height 110)
(set-face-attribute 'variable-pitch nil :family "NotoSans NF" :height 115)
(set-face-attribute 'variable-pitch-text nil :height 115)
(set-face-attribute 'italic nil :slant 'italic :underline nil)
(set-fontset-font t 'symbol "Noto Color Emoji")

(unless (package-installed-p 'nerd-icons)
  (package-install 'nerd-icons))

(setq nerd-icons-font-family "NotoSansM Nerd Font")

(defun +subtle-whitespace ()
  (interactive)
  (dolist
      (face
       '(whitespace-space
	 whitespace-tab
	 whitespace-newline
	 whitespace-indentation))
    (set-face-attribute
     face
     nil
     :background (face-attribute 'default :background))))

(setq whitespace-action '(warn-if-read-only auto-cleanup)
      whitespace-style
      '(face tabs spaces trailing lines-tail newline empty indentation
	     big-indent help-newline tab-mark space-mark newline-mark))
(add-hook 'whitespace-mode-hook '+subtle-whitespace)

(setopt whitespace-line-column 80)

(setq display-line-numbers-type 'relative)

(unless (package-installed-p 'gruber-darker-theme)
  (package-install 'gruber-darker-theme))

(unless (package-installed-p 'standard-themes)
  (package-install 'standard-themes))

(setq standard-themes-mixed-fonts t
      standard-themes-headings
      '((0 . (1.5))
	(1 . (1.4))
	(2 . (1.3))
	(3 . (1.2))
	(4 . (1.1))))

(add-hook 'standard-themes-post-load-hook #'+subtle-whitespace)

(unless (package-installed-p 'ef-themes)
  (package-install 'ef-themes))

(setq ef-themes-to-toggle '(ef-dark ef-summer)
      ef-themes-mixed-fonts t
      ef-themes-headings
      '((0 . (1.5))
	(1 . (1.4))
	(2 . (1.3))
	(3 . (1.2))
	(4 . (1.1))))

(add-hook 'ef-themes-post-load-hook '+subtle-whitespace)

(unless (package-installed-p 'auto-dark)
  (package-install 'auto-dark))

(setq auto-dark-dark-theme 'modus-vivendi
      auto-dark-light-theme 'modus-operandi
      auto-dark-allow-osascript t)

(defun +auto-dark-enable-once (_frame)
  (require 'auto-dark)
  (auto-dark-mode 1)
  (remove-hook 'after-make-frame-functions #'+auto-dark-enable-once))

(add-hook 'auto-dark-dark-mode-hook #'+subtle-whitespace)
(add-hook 'auto-dark-light-mode-hook #'+subtle-whitespace)
(add-hook 'after-make-frame-functions #'+auto-dark-enable-once)
(add-hook 'modus-themes-after-load-theme-hook '+subtle-whitespace)

(unless (package-installed-p 'mixed-pitch)
  (package-install 'mixed-pitch))

(provide '+faces)
;;; +faces.el ends here
