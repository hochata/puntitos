;;; +frame.el --- frame configuration                -*- lexical-binding: t; -*-

;;; Code:

(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode 1)
(column-number-mode 1)
(setq inhibit-splash-screen t
      scroll-conservatively 101
      ring-bell-function #'ignore)

;; yabai fails to manage Emacs after a restart
(defun +yabai-restart-once (_frame)
  (when (executable-find "yabai")
    (async-shell-command "yabai --restart-service"))
  (remove-hook 'after-make-frame-functions #'+yabai-restart-once))

(add-hook 'after-make-frame-functions #'+yabai-restart-once)

(provide '+frame)
;;; +frame.el ends here
