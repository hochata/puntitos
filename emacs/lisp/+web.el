;;; +web.el --- web browsing for emacs     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <hochata@disroot.org>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(setq browse-url-browser-function 'eww-browse-url
      browse-url-handlers
      '(("^\\(gopher\\|gemini\\)://.*$"
	 . elpher-browse-url-elpher)
	("https://\\(youtu.be\\|m\\.youtube\\)"
	 . +empv-browse-url)))

(defun +empv-browse-url (url &rest args)
  (empv-play url))

(unless (package-installed-p 'osm)
  (package-install 'osm))

(unless (package-installed-p 'elpher)
  (package-install 'elpher))

(add-hook 'eww-mode-hook 'visual-line-mode)

(unless (package-installed-p 'elfeed-tube)
  (package-install 'elfeed-tube))

(unless (package-installed-p 'elfeed-tube-mpv)
  (package-install 'elfeed-tube-mpv))

(autoload
  'elfeed-tube-setup
  "elfeed-tube" "Setup yt integration for elfeed"
  t)
(add-hook 'elfeed-update-init-hooks 'elfeed-tube-setup)

(autoload 'elfeed-tube-mpv "elfeed-tube-mpv" "Start of connection to MPV session" t)

(with-eval-after-load 'elfeed
  (define-key elfeed-show-mode-map (kbd "RET") 'elfeed-tube-mpv))

(unless (package-installed-p 'elfeed)
  (package-install 'elfeed))

(defun +elfeed-bring ()
  (interactive)
  (elfeed)
  (elfeed-search-buffer))

(with-eval-after-load '+buffers
  (+yeq-add "Elfeed" '+elfeed-bring))

(unless (package-installed-p 'empv)
  (package-install 'empv))
(setopt empv-invidious-instance "https://inv.tux.pizza/api/v1"
	empv-youtube-use-tabulated-results t)
(with-eval-after-load 'empv
  (add-to-list 'empv-mpv-args "--ytdl-format=best"))

(provide '+web)
;;; +web.el ends here
