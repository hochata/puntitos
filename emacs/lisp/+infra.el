;;; +infra.el --- infrastructure related packages    -*- lexical-binding: t; -*-

;;; Code:

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(dockerfile . ("https://github.com/camdencheek/tree-sitter-dockerfile")))
  (add-to-list 'treesit-language-source-alist
	       '(yaml . ("https://github.com/ikatyang/tree-sitter-yaml")))

  (add-to-list 'treesit-language-source-alist
	       '(terraform
		 . ("https://github.com/MichaHoffmann/tree-sitter-hcl"
		    "main"
		    "dialects/terraform/src"))))

(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-ts-mode))
(add-to-list 'auto-mode-alist '("\\.y[a]?ml\\'" . yaml-ts-mode))
(unless (package-installed-p 'terraform-ts-mode)
  (package-vc-install "https://github.com/kgrotel/terraform-ts-mode"))
(autoload 'terraform-ts-mode "terraform-ts-mode")

(add-to-list 'auto-mode-alist '("\\.tf\\'" . terraform-ts-mode))

(unless (package-installed-p 'flymake-sqlfluff)
  (package-install 'flymake-sqlfluff))

(unless (package-installed-p 'sql-indent)
  (package-install 'sql-indent))

(unless (package-installed-p 'docker)
  (package-install 'docker))

(setq sql-dialect 'postgres)
(setq-default sqlind-basic-offset 4)

(add-hook 'sql-mode-hook #'flymake-sqlfluff-load)
(add-hook 'sql-mode-hook #'flymake-mode)
(add-hook 'sql-mode-hook #'apheleia-mode-maybe)
(add-hook 'sql-mode-hook (lambda () (indent-tabs-mode -1)))

(defun +to-sqlfluff-dialect (dialect)
  (pcase dialect
    ('ansi "ansi")
    ('db2 "db2")
    ('mariadb "mysql")
    ('mysql "mysql")
    ('oracle "oracle")
    ('postgres "postgres")
    ('sqlite "sqlite")
    (_ (error "Can not translate to a valid SQLFluff dialect"))))

(with-eval-after-load 'apheleia
  (add-to-list 'apheleia-formatters
	       '(sqlfluff "sqlfluff"
			  "format"
			  "--nocolor"
			  "--disable-progress-bar"
			  "--dialect" (+to-sqlfluff-dialect sql-dialect)
			  "-"))
  (add-to-list 'apheleia-mode-alist '(sql-mode . sqlfluff)))

(provide '+infra)
;;; +infra.el ends here
