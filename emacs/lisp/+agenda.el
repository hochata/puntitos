;;; +agenda.el --- agenda and calendar               -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(setq org-todo-keywords
      '("TODO(t)" "NEXT(n)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)"))

(setq org-log-done 'time
      org-log-into-drawer t
      org-log-state-notes-into-drawer t
      org-log-done-with-time t)

(unless (package-installed-p 'calfw)
  (package-install 'calfw))

(unless (package-installed-p 'calfw-org)
  (package-install 'calfw-org))

(autoload
  'cfw:open-org-calendar
  "calfw-org"
  "Open an org schedule calendar in the new buffer"
  t)

(define-key global-map (kbd "C-x C-c") 'cfw:open-org-calendar)

(with-eval-after-load 'calfw-org
  (define-key
   cfw:calendar-mode-map
   (kbd "RET")
   'cfw:show-details-command))

(with-eval-after-load 'meow
  (add-to-list
   'meow-mode-state-list
   '(cfw:calendar-mode . motion)))

(unless (package-installed-p 'org-caldav)
  (package-install 'org-caldav))

(setq org-icalendar-timezone "America/Mexico_City")

(provide '+agenda)
;;; +agenda.el ends here
