;;; +js.el --- general configuration for JS code     -*- lexical-binding: t; -*-

;;; Code:

(setq js-indent-level 2)

(nconc auto-mode-alist
       '(("\\.m?js\\'" . js-ts-mode)
	 ("\\.json\\'" . json-ts-mode)
	 ("\\.m?ts\\'" . typescript-ts-mode)
	 ("\\.tsx\\'" . tsx-ts-mode)))

(nconc major-mode-remap-alist
       '((js-json-mode . json-ts-mode)
	 (js-mode . js-ts-mode)))

(with-eval-after-load 'treesit
  (nconc treesit-language-source-alist
	 '((javascript "https://github.com/tree-sitter/tree-sitter-javascript")
	   (json "https://github.com/tree-sitter/tree-sitter-json")
	   (typescript
	    "https://github.com/tree-sitter/tree-sitter-typescript"
	    "master"
	    "typescript/src")
	   (tsx
	    "https://github.com/tree-sitter/tree-sitter-typescript"
	    "master"
	    "tsx/src"))))

(with-eval-after-load 'project
  (add-to-list 'project-vc-extra-root-markers "package.json")
  (nconc project-vc-ignores
	 '("*/dist/*" "*/codegen/*" "*/node_modules/*" "*/build/*")))

(defun +js-add-node-modules ()
  (let ((node-root (string-trim (shell-command-to-string "npm root"))))
    (when (file-directory-p node-root)
      (setq-local
       exec-path
       (append
	exec-path
	(list
	 (file-name-concat node-root ".bin")))))))

(defun +js-hooks ()
  (setq-local tab-width typescript-ts-mode-indent-offset)
  (indent-tabs-mode -1)
  (apheleia-mode-maybe)
  (jest-test-mode)
  (+js-add-node-modules)
  (eglot-ensure))

(add-hook 'typescript-ts-mode-hook #'+js-hooks)
(add-hook 'tsx-ts-mode-hook #'+js-hooks)
(add-hook 'js-ts-mode-hook #'+js-hooks)
(add-hook 'json-ts-mode-hook (lambda () (indent-tabs-mode -1)))

(unless (package-installed-p 'flymake-eslint)
  (package-install 'flymake-eslint))

(setq flymake-eslint-prefer-json-diagnostics t)

(unless (package-installed-p 'eslint-disable-rule)
  (package-install 'eslint-disable-rule))

(setq eslint-disable-rule-require-description nil)

(add-hook 'eglot-managed-mode-hook
	  (lambda ()
	    (when (derived-mode-p 'typescript-ts-mode 'tsx-ts-mode 'js-ts-mode)
	      (flymake-eslint-enable))))

(unless (package-installed-p 'jest-test-mode)
  (package-install 'jest-test-mode))

(unless (package-installed-p 'css-in-js-mode)
  (package-vc-install
   '(css-in-js-mode
     :url "https://github.com/llemaitre19/tree-sitter-css-in-js")))

;; https://github.com/svaante/dape/issues/15#issuecomment-1793808378
(defcustom dape-configs-js-debug-version "v1.87.0" "dape js-debug version")

(defun dape-configs--ensure-dir ()
  (unless (file-directory-p dape-adapter-dir)
    (make-directory dape-adapter-dir)))

(defun dape-configs--download (url &optional filename)
  (dape-configs--ensure-dir)
  (let ((file-name (or filename (car (last (split-string url "/")))))
	(default-directory dape-adapter-dir))
    (url-copy-file url file-name t)
    (dired-compress-file file-name)
    (delete-file file-name)))

(defun dape-configs-download-js-debug ()
  "Download js-debug."
  (interactive)
  (dape-configs--download
   (format
    "https://github.com/microsoft/vscode-js-debug/releases/download/%s/js-debug-dap-%s.tar.gz"
    dape-configs-js-debug-version dape-configs-js-debug-version)))

(with-eval-after-load 'dape
  (let ((js-debug-ts-node (alist-get 'js-debug-ts-node dape-configs)))
    (add-to-list
     'dape-configs
     `(js-debug-ts-attach
       ,@js-debug-ts-node
       :runtimeExecutable "tsx"
       :request "attach"
       :port 9229))))

(provide '+js)
;;; +js.el ends here
