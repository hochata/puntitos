;;; +llm.el --- LLM assistants                       -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Edgar Quiroz

;; Author: Edgar Quiroz <edgarquiroz@192.168.1.5>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(unless (package-installed-p 'copilot)
  (package-vc-install "https://github.com/copilot-emacs/copilot.el"))

(with-eval-after-load 'copilot
  (define-key copilot-completion-map (kbd "RET") 'copilot-accept-completion)
  (define-key copilot-mode-map (kbd "C-<tab>") 'copilot-complete)
  (add-to-list 'copilot-major-mode-alist '("typescript-ts" . "typescript")))

(setq copilot-idle-delay nil)

(unless (package-installed-p 'ellama)
  (package-install 'ellama))

(unless (package-installed-p 'elisa)
  (package-vc-install "https://github.com/s-kostyaev/elisa"))

(provide '+llm)
;;; +llm.el ends here
