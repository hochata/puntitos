;;; +python.el --- python configuration              -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(with-eval-after-load 'treesit
  (add-to-list
   'treesit-language-source-alist
   '(python "https://github.com/tree-sitter/tree-sitter-python/")))

(add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode))

(setq python-shell-dedicated 'project)

(unless (package-installed-p 'poetry)
  (package-install 'poetry))

(unless (package-installed-p 'pyvenv)
  (package-install 'pyvenv))

(with-eval-after-load 'project
  (add-to-list 'project-vc-extra-root-markers "requirements.txt"))

(provide '+python)
;;; +python.el ends here
