;;; +webdev.el --- extra configurations for web dev            -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(setq css-indent-offset 2)

(with-eval-after-load 'treesit
  (add-to-list
   'treesit-language-source-alist
   '(html "https://github.com/tree-sitter/tree-sitter-html"))
  (add-to-list
   'treesit-language-source-alist
   '(css "https://github.com/tree-sitter/tree-sitter-css")))

(add-to-list 'major-mode-remap-alist '(html-mode . html-ts-mode))
(add-to-list 'major-mode-remap-alist '(css-mode . css-ts-mode))

(unless (package-installed-p 'web-mode)
  (package-install 'web-mode))

(add-to-list 'auto-mode-alist '("\\.dj\\.html\\'" . web-mode))

(with-eval-after-load 'web-mode
  (set-face-attribute
   'web-mode-html-tag-face
   nil
   :inherit 'font-lock-constant-face
   :foreground nil)
  (set-face-attribute
   'web-mode-block-control-face
   nil
   :inherit 'font-lock-keyword-face)
  (set-face-attribute
   'web-mode-doctype-face
   nil
   :inherit 'font-lock-builtin-face
   :foreground nil)
  (add-to-list 'web-mode-engines-alist '("django" . "\\.dj\\.html\\'")))

(add-hook 'js-json-mode-hook (lambda () (indent-tabs-mode -1)))

(provide '+webdev)
;;; +webdev.el ends here
